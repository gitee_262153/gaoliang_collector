# gaoliang采集器

#### 介绍
gaoliang采集器是一款基于java语言开发的web采集器，分为WEB后台管理，爬虫框架（独立版）

#### 软件架构
软件架构说明
分为WEB后台管理，采集端采用动态采集规则采集！目前开放版本1.0
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/221856_0ed80a7f_4923370.png "采集规则管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222204_c55ff4bb_4923370.png "采集节点管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222219_657063c1_4923370.png "采集器管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222226_331db6ca_4923370.png "采集图片管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222236_a60e83b7_4923370.png "采集文件管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222242_db49d596_4923370.png "采集文章管理1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222253_c30d1fbb_4923370.png "采集文章管理2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222309_2205c6cb_4923370.png "采集文章详情1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222319_dfba72dd_4923370.png "采集详情2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222329_33d95090_4923370.png "导出器管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222338_7f045834_4923370.png "导出器详情.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222347_15fc1da4_4923370.png "导出数据源管理.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222356_064fe012_4923370.png "导出图片管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222404_6e033e54_4923370.png "导出文件管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222414_3c87c62c_4923370.png "导出文章管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222437_2811c990_4923370.png "规则详情1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222445_f55af1f5_4923370.png "规则详情2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222458_d2562b16_4923370.png "首页.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222506_150b0556_4923370.png "添加导出数据源.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0516/222515_dbbe5bc3_4923370.png "系统管理.png")


#### 安装教程

传不了超过10M的文件，搞毛？
1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技
qq:3328350766
email:3328350766@qq.com
1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)